//
//  ChatViewControlerViewController.h
//  InstantChat
//
//  Created by Andre Vieira on 8/10/15.
//  Copyright (c) 2015 Andre Vieira. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) NSString *chatMateId;
@property (strong, nonatomic) NSMutableArray* messageArray;
@property (strong, nonatomic) NSString *myUserId;
@property (strong, nonatomic) UITextField *activeTextField;

@end
