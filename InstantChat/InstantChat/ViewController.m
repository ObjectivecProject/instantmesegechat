//
//  ViewController.m
//  InstantChat
//
//  Created by Andre Vieira on 8/3/15.
//  Copyright (c) 2015 Andre Vieira. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UITextField *usernameField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property (weak, nonatomic) IBOutlet UILabel *promptLabel;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)signup:(id)sender {
    PFUser *pfUser = [PFUser user];
    pfUser.username = self.usernameField.text;
    pfUser.password = self.passwordField.text;
    
    __weak typeof(self) weakSelf = self;
    [pfUser signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            weakSelf.promptLabel.textColor = [UIColor greenColor];
            weakSelf.promptLabel.text = @"Signup successful!";
            weakSelf.promptLabel.hidden = NO;
            [weakSelf login:nil];
            
        } else {
            weakSelf.promptLabel.textColor = [UIColor blueColor];
            weakSelf.promptLabel.text = [error userInfo][@"error"];
            weakSelf.promptLabel.hidden = NO;
        }
    }];
}

- (IBAction)login:(id)sender {
    __weak typeof(self) weakSelf = self;
    
    [PFUser logInWithUsernameInBackground:self.usernameField.text
                                 password:self.passwordField.text
                                    block:^(PFUser *pfUser, NSError *error)
     {
         if (pfUser && !error) {
             weakSelf.promptLabel.hidden = YES;
             AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
             [appDelegate initSinchClient:self.usernameField.text];
             
             UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
             ChatListViewController *vc = [sb instantiateViewControllerWithIdentifier:@"ChatListViewController"];
             [self.navigationController pushViewController:vc animated:YES];
             
         } else {
             weakSelf.promptLabel.textColor = [UIColor redColor];
             weakSelf.promptLabel.text = [NSString stringWithFormat:@"%@ %@", [error userInfo], @"error"];
             weakSelf.promptLabel.hidden = NO;
         }
     }];
    
}

@end
