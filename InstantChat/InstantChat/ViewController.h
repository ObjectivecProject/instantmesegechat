//
//  ViewController.h
//  InstantChat
//
//  Created by Andre Vieira on 8/3/15.
//  Copyright (c) 2015 Andre Vieira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Sinch/Sinch.h>
#import <Parse/Parse.h>
#import "AppDelegate.h"
#import "ChatListViewController.h"

@interface ViewController : UIViewController


@end

