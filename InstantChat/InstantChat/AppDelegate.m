//
//  AppDelegate.m
//  InstantChat
//
//  Created by Andre Vieira on 8/3/15.
//  Copyright (c) 2015 Andre Vieira. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [Parse enableLocalDatastore];
    [Parse setApplicationId:PARSE_APPLICATION_ID clientKey:PARSE_CLIENT_KEY];
    [PFAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
}

- (void)applicationWillTerminate:(UIApplication *)application {
}

- (void)initSinchClient:(NSString*)userId{
    self.sinchClient = [Sinch clientWithApplicationKey:SINCH_APPLICATION_KEY
                                   applicationSecret:SINCH_APPLICATION_SECRET
                                     environmentHost:SINCH_ENVIRONMENT_HOST
                                              userId:userId];
    
    self.sinchClient.delegate = self;
    [self.sinchClient setSupportMessaging:YES];
    [self.sinchClient start];
    [self.sinchClient startListeningOnActiveConnection];
}

@end
