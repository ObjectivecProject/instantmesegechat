//
//  ChatListViewController.h
//  InstantChat
//
//  Created by Andre Vieira on 8/9/15.
//  Copyright (c) 2015 Andre Vieira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import <Sinch/Sinch.h>
#import "Config.h"

@interface ChatListViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) NSMutableArray *chatArray;

@end
