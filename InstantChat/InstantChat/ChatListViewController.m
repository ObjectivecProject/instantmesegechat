//
//  ChatListViewController.m
//  InstantChat
//
//  Created by Andre Vieira on 8/9/15.
//  Copyright (c) 2015 Andre Vieira. All rights reserved.
//

#import "ChatListViewController.h"

@interface ChatListViewController ()

@property(weak, nonatomic) IBOutlet UITableView *chatListTableView;

@end

@implementation ChatListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.chatListTableView.dataSource = self;
    self.chatListTableView.delegate = self;
    
    self.chatArray = [[NSMutableArray alloc] init];
    
    [self retrivechatFromParse];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)retrivechatFromParse{
    [self.chatArray removeAllObjects];
    
    PFQuery *query = [PFUser query];
    [query orderByAscending:@"username"];
    [query whereKey:@"username" notEqualTo:@"andre"];
    
    __weak typeof(self) weakSelf = self;
    [query findObjectsInBackgroundWithBlock:^(NSArray *chatMateArray, NSError *error) {
        if (!error) {
            for (int i = 0; i < [chatMateArray count]; i++) {
                [weakSelf.chatArray addObject:chatMateArray[i][@"username"]];
            }
            [weakSelf.chatListTableView reloadData];
        } else {
            NSLog(@"Error: %@", error.description);
        }
    }];
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.chatArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    cell.textLabel.text = [self.chatArray objectAtIndex:indexPath.row];
    
    return cell;
}


@end
